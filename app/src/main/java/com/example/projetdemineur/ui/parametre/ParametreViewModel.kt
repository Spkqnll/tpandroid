package com.example.projetdemineur.ui.parametre

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.projetdemineur.data.repositories.AuthRepository



class ParametreViewModel() : ViewModel() {

    val _text = MutableLiveData<String>().apply {
        value = "This is parametre Fragment"
    }
    val text: LiveData<String> = _text


}

