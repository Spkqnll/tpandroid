package com.example.projetdemineur.ui.adapters

import android.graphics.Color
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.projetdemineur.R
import com.example.projetdemineur.ui.jeu.JeuFragment
import com.example.projetdemineur.ui.jeu.JeuViewModel


class GridAdapter(private val cases:List<Case>, parentFragment: JeuFragment) : RecyclerView.Adapter<GridAdapter.CaseViewHolder>() {
    private lateinit var view:CaseViewHolder
    val parentFragment = parentFragment
    var chrono: Boolean = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CaseViewHolder {
        val tmp = LayoutInflater.from(parent.context).inflate(R.layout.item_case, parent, false)
        return CaseViewHolder(tmp)

    }

    override fun getItemCount(): Int {
        return cases.size
    }

    override fun onBindViewHolder(holder: GridAdapter.CaseViewHolder, position: Int) {
        val event = cases.get(position)
        holder.updateEvent(event)
    }

     inner class CaseViewHolder(item:View): RecyclerView.ViewHolder(item) {
         val btnDefault = item.findViewById<Button>(R.id.defaultCase)
         val flag = item.findViewById<Button>(R.id.flag)
         val vide = item.findViewById<View>(R.id.vide)
         val number = item.findViewById<TextView>(R.id.nbBombes)
         val bombe = item.findViewById<ImageView>(R.id.bombe)
         fun updateEvent(event: Case) {
             btnDefault.setOnClickListener {
                 btnDefault.visibility = View.INVISIBLE
                 if(event.bombe) {
                     bombe.visibility = View.VISIBLE
                     parentFragment.clickOnBomb()
                 } else if (event.nbBombe>0) {
                     number.visibility = View.VISIBLE
                     number.text = event.nbBombe.toString()
                 } else {
                    decouvre(event)
                 }
                 if(!chrono){
                     chrono = true
                     parentFragment.startChrono()
                 }
             }
             btnDefault.setOnLongClickListener {
                 btnDefault.visibility = View.INVISIBLE
                 flag.visibility = View.VISIBLE
                 parentFragment.addFlag(event.id)
                 true
             }

             flag.setOnLongClickListener {
                 btnDefault.visibility = View.VISIBLE
                 flag.visibility = View.INVISIBLE
                 parentFragment.removeFlag(event.id)
                 true
             }
         }

         fun decouvre(event: Case){
             btnDefault.visibility = View.INVISIBLE
             vide.visibility = View.VISIBLE
             event.decouvert = true
             if(event.id > 7 && !cases[event.id-8].decouvert) {
                 parentFragment.grid.findViewHolderForAdapterPosition(event.id-8)?.itemView?.findViewById<Button>(R.id.defaultCase)?.performClick()
             }
             if(event.id < 88 && !cases[event.id+8].decouvert){
                 parentFragment.grid.findViewHolderForAdapterPosition(event.id+8)?.itemView?.findViewById<Button>(R.id.defaultCase)?.performClick()
             }
             if(event.id%8 != 0 && !cases[event.id-1].decouvert){
                 parentFragment.grid.findViewHolderForAdapterPosition(event.id-1)?.itemView?.findViewById<Button>(R.id.defaultCase)?.performClick()
             }
             if(event.id%8 != 7 && !cases[event.id+1].decouvert){
                 parentFragment.grid.findViewHolderForAdapterPosition(event.id+1)?.itemView?.findViewById<Button>(R.id.defaultCase)?.performClick()
             }
         }
     }
}