package com.example.projetdemineur.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.projetdemineur.data.repositories.AuthRepository
import com.google.firebase.auth.FirebaseUser

class RegisterViewModel(val authRepository: AuthRepository) : ViewModel() {

    val _text = MutableLiveData<String>().apply {
        value = "This is Register Fragment"
    }
    val text: LiveData<String> = _text

    fun signIn(email: String, password: String) = authRepository.signIn(email,password)
    fun signUp(email: String, password: String) = authRepository.signUp(email, password)


}