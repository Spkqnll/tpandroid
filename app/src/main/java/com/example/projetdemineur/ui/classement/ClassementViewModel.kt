package com.example.projetdemineur.ui.classement

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ClassementViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is classement Fragment"
    }
    val text: LiveData<String> = _text
}