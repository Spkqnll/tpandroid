package com.example.projetdemineur.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projetdemineur.R

class ClassementAdapter (private val scores:MutableList<Int>) : RecyclerView.Adapter<ClassementAdapter.ScoreViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoreViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_score, parent, false)
        return ScoreViewHolder(view)
    }


    override fun getItemCount(): Int {
        return scores.size
    }

    override fun onBindViewHolder(holder: ClassementAdapter.ScoreViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ScoreViewHolder(item: View): RecyclerView.ViewHolder(item) {
        fun onBind(){

        }
    }
}