package com.example.projetdemineur.ui.register

import android.Manifest
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.projetdemineur.R
import com.example.projetdemineur.ui.parametre.ParametreViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.w3c.dom.Text

class RegisterFragment : Fragment() {

    private val registerViewModel: RegisterViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_register, container, false)

        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        view.findViewById<TextView>(R.id.buttonSignIn).setOnClickListener {
            val editTextEmail = view.findViewById<EditText>(R.id.text_register)
            val editTextPassword = view.findViewById<EditText>(R.id.text_password)


            //Get the editTexts values
            registerViewModel.signUp(editTextEmail.text.toString(), editTextPassword.text.toString())
                .observe(viewLifecycleOwner, Observer {
                    it?.let { firebaseUser ->
                        firebaseUser.uid
                        Toast.makeText(view.context, "Register successfully",Toast.LENGTH_SHORT).show()
                        view.findNavController().navigate(R.id.navigation_jeu)
                    }
                })

            registerViewModel.signIn(editTextEmail.text.toString(), editTextPassword.text.toString())
                .observe(viewLifecycleOwner, Observer {
                    it?.let { firebaseUser ->
                        firebaseUser.uid
                        Log.d("EmailPassword", "signInWithEmail:success")
                        Toast.makeText(view.context, "SignIn successfully",Toast.LENGTH_SHORT).show()
                        view.findNavController().navigate(R.id.navigation_jeu)
                    }
                })


        }

    }
}
