package com.example.projetdemineur.ui.classement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projetdemineur.R
import com.example.projetdemineur.ui.adapters.ClassementAdapter
import com.google.firebase.ktx.Firebase

class ClassementFragment : Fragment() {

    private lateinit var classementViewModel: ClassementViewModel
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        classementViewModel =
                ViewModelProvider(this).get(ClassementViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_classement, container, false)
        return root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val classement: RecyclerView = view.findViewById(R.id.recyclerClassement)
        val testArray = mutableListOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20)
        classement.adapter = ClassementAdapter(testArray)
        classement.layoutManager = GridLayoutManager(requireContext(), 1, RecyclerView.VERTICAL, false)
    }
}