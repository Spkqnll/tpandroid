package com.example.projetdemineur.ui

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import com.example.projetdemineur.ui.MainActivity
import com.example.projetdemineur.R

class PopUpWindow : AppCompatActivity() {
    private var popupTitle = ""
    private var popupText = ""
    private var popupButton = ""
    private var darkStatusBar = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(0, 0)
        setContentView(R.layout.activity_pop_up_window)

        val bundle = intent.extras
        popupTitle = bundle?.getString("popuptitle", "Title") ?: ""
        popupText = bundle?.getString("popuptext", "Text") ?: ""
        popupButton = bundle?.getString("popupbtn", "Button") ?: ""
        darkStatusBar = bundle?.getBoolean("darkstatusbar", false) ?: false

        val popup_window_title = findViewById<TextView>(R.id.popup_window_title)
        val popup_window_text = findViewById<TextView>(R.id.popup_window_text)
        val popup_window_button = findViewById<TextView>(R.id.popup_window_button)

        popup_window_title.text = popupTitle
        popup_window_text.text = popupText
        popup_window_button.text = popupButton
        popup_window_button.setOnClickListener {
            val intent = Intent(baseContext, MainActivity::class.java)
            startActivity(intent)
        }
    }

}