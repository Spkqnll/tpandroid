package com.example.projetdemineur.ui.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.projetdemineur.ui.MainActivity

class Case(private var setBombe:Boolean, private var setId: Int) {
    var flag: Boolean = false
    var nbBombe: Int = 0
    var bombe: Boolean = setBombe
    var decouvert: Boolean = false
    var id: Int = setId
}