package com.example.projetdemineur.ui.jeu

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.ClipData
import android.content.ContentValues.TAG
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.os.SystemClock
import android.transition.Slide
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.inflate
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.res.ComplexColorCompat.inflate
import androidx.core.graphics.drawable.DrawableCompat.inflate
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projetdemineur.R
import com.example.projetdemineur.ui.adapters.Case
import com.example.projetdemineur.ui.adapters.GridAdapter
import kotlin.random.Random
import com.example.projetdemineur.ui.MainActivity

import com.example.projetdemineur.ui.PopUpWindow
import com.google.android.gms.location.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.lang.Exception
import java.util.*

class JeuFragment : Fragment() {
    private lateinit var jeuViewModel: JeuViewModel
    var bombes: Int =15
    var time: Boolean = false
    var list = mutableListOf<Int>()
    var listBombe:List<Int> = getBombes()
    lateinit var grid: RecyclerView
    lateinit var nbBombes: TextView
    lateinit var chrono: Chronometer
    var start = false
    val PERMISSION_ID = 1
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var location: Location
    lateinit var cityName: String

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        jeuViewModel =
                ViewModelProvider(this).get(JeuViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_jeu, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationProviderClient.lastLocation.addOnCompleteListener {
            Log.e(TAG, "${it.result?.longitude} ${it.result?.latitude}")
            var lat = it.result?.latitude
            var long = it.result?.longitude
            var geocoder = Geocoder(requireContext(), Locale.getDefault())
            var a = geocoder.getFromLocation(lat!!, long!!, 3)
            cityName = a.get(0).locality
            Log.e(TAG, "city : ${cityName}")
        }
        nbBombes = view.findViewById<TextView>(R.id.nbBombes)
        nbBombes.setText(bombes.toString())
        grid = view.findViewById(R.id.recyclerGrid)
        chrono = view.findViewById<Chronometer>(R.id.chronometer)
        var arrayCase: List<Case> = List(96) {i ->
            if(listBombe.contains(i)) {
                Case(true, i)
            }else {
                Case(false, i)
            }
        }
        var arrayTable: List<List<Case>>
        arrayTable = List(12){i->
            List(8){j->
                if(listBombe.contains(i*8+j)){
                    Case(true, i*8+j)
                } else {
                    Case(false, i*8+j)
                }
            }
        }
        arrayTable = setNbBombes(arrayTable)
        grid.adapter = GridAdapter(arrayTable.flatten(), this)
        grid.layoutManager = GridLayoutManager(requireContext(), 8, RecyclerView.VERTICAL, false)
    }

    fun getBombes():List<Int> {
        var randomValues = List(bombes) { Random.nextInt(96) }
        while(randomValues.distinct().count() != bombes) {
            randomValues = List(bombes) { Random.nextInt(96) }
        }
        return randomValues.sorted()
    }

    fun setNbBombes(arrayTable: List<List<Case>>):List<List<Case>> {
        //bombe premier
        val nbBombe = 0
        arrayTable[0][0].nbBombe = getBombe(arrayTable[1][0].bombe)+getBombe(arrayTable[0][1].bombe)+getBombe(arrayTable[1][1].bombe)
        arrayTable[11][0].nbBombe = getBombe(arrayTable[10][0].bombe)+getBombe(arrayTable[10][1].bombe)+getBombe(arrayTable[11][1].bombe)
        arrayTable[0][7].nbBombe = getBombe(arrayTable[0][6].bombe)+getBombe(arrayTable[1][6].bombe)+getBombe(arrayTable[1][7].bombe)
        arrayTable[11][7].nbBombe = getBombe(arrayTable[11][6].bombe)+getBombe(arrayTable[10][6].bombe)+getBombe(arrayTable[10][7].bombe)
        for(i in 1..6){
            arrayTable[0][i].nbBombe = getBombe(arrayTable[0][i+1].bombe) + getBombe(arrayTable[0][i-1].bombe) + getBombe(arrayTable[1][i].bombe) + getBombe(arrayTable[1][i+1].bombe) + getBombe(arrayTable[1][i-1].bombe)
            arrayTable[11][i].nbBombe = getBombe(arrayTable[11][i+1].bombe) + getBombe(arrayTable[11][i-1].bombe) + getBombe(arrayTable[10][i].bombe) + getBombe(arrayTable[10][i+1].bombe) + getBombe(arrayTable[10][i-1].bombe)
        }
        for(i in 1..10){
            arrayTable[i][0].nbBombe = getBombe(arrayTable[i+1][0].bombe) + getBombe(arrayTable[i-1][0].bombe) + getBombe(arrayTable[i][1].bombe) + getBombe(arrayTable[i+1][1].bombe) + getBombe(arrayTable[i-1][1].bombe)
            arrayTable[i][7].nbBombe = getBombe(arrayTable[i+1][7].bombe) + getBombe(arrayTable[i-1][7].bombe) + getBombe(arrayTable[i][6].bombe) + getBombe(arrayTable[i+1][6].bombe) + getBombe(arrayTable[i-1][6].bombe)
        }
        for(i in 1..10){
            for(j in 1..6){
                arrayTable[i][j].nbBombe =
                        getBombe(arrayTable[i-1][j-1].bombe) +
                        getBombe(arrayTable[i-1][j+1].bombe) +
                        getBombe(arrayTable[i+1][j+1].bombe) +
                        getBombe(arrayTable[i+1][j-1].bombe) +
                        getBombe(arrayTable[i][j-1].bombe) +
                        getBombe(arrayTable[i][j+1].bombe) +
                        getBombe(arrayTable[i-1][j].bombe) +
                        getBombe(arrayTable[i+1][j].bombe)
            }
        }
        return arrayTable
    }

    fun addFlag(i:Int){
        list.add(i)
        list.sort()
        bombes--
        nbBombes.setText(bombes.toString())
        println(list)
        println(listBombe)
        if(1 == 1){
            println("the end")
            val myPop: AlertDialog.Builder = AlertDialog.Builder(activity)
            myPop.setTitle("GG")
            myPop.setMessage("Tu as gagné")
            myPop.setPositiveButton("Recommencer", DialogInterface.OnClickListener(
                fun(dialogInterface: DialogInterface, i: Int){
                    Toast.makeText(context, "Recommencer", Toast.LENGTH_SHORT).show()
                    view?.findNavController()?.navigate(R.id.navigation_jeu)
                    val db = Firebase.firestore
                    chrono.stop()
                    val tps = chrono.text.split(":")
                    val tpsScore = tps[0].toInt() * 60 + tps[1].toInt()
                    Log.e(TAG, "time ${tpsScore}")
                    RequestPermission()
                    val score = hashMapOf(
                        "score" to "${tpsScore}",
                        "place" to "${cityName}",
                        "name" to "name",
                        "image" to "iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAgAElEQVR4nOy9B4xkSXom9kXEs+mzsmx7Nz3T49cvQbNcLsm9O4ogBAqUSOpEQeYoUiKPEiSdCOkOEiDR4KA7UQcecCeeKENRS0A0AriSaIdHct0Md2d3x5v23eVN+sznIoT/j3hZ1T0ze2O6e6pxE41Cd1VnvXz54o/ffv/344P1wfpgfbA+WB+sD9YH64P1wfpg/Uu2xJ/+7q8eqk+8t7cLoxWyLEO3t416vYEwDBGGEYSMcfPmDWzvbOP48aPwPA9pmmI6nSJJEwTKQxTGGI7G6PUH6My1EVeq6Mw1MRgN/c3NzYVTJ88em0ynH7n4+usfzdLkwTiOLyjl1aIo/Pqp02e+0Go3P7+2dvObWuutfm+IpaVFdObb6PW6KIoCC4uL0BpYX91EluUQUhyCp/bul3e/3vjbWQLwjdF/q9vrf3wyTc4OR5PzTz/z1QWtNa5fv47BYMBC5AcBgiD4+OrG9sdPHj/2HxsU15Np8rRS8kvGmBcBc1UIsSWlHCmlxsbow//h3+a6bwVASslfQggphICUgr9X9udGKVVd29j81dF4/G9BCuRFgSI3yHODvb09dLt9PtF6ksIPckRhgVdfu4TRaIJaLT7e7e4dB8wP73V7WFlZLoLQ73pK3ZTKu9Rqtf+uEOL5Q/AY3vO6bwTAbrLsCCHOGWPO9rq9s2EYnddGH03TDKPRGEMNRGGCJEmLnb29+a3t7ScnSYqi0JhMxigKII4aMMZAeTEKk0EXGaZJAQiNMAywsbWDnV3wawCDwWiMtc1N1ajXO8aYjuepx0+fPvVop9X5V5Wn7nshuC8EQCnlDwaDv7+1s/ZjW1ubja2trfDqteuoxDGrcFLpo/EYk8nU2mWlkOcFJtME2oAFoCgMCq2R9AesKWjRJhvBWgR5YSCynH4IzwNrB/ItpAKSJEOvPwKgkeU5Ll26fO7hhy783rFjx7+tKPKt9/0BvYd1qAWANgZAbX19/dcuXrryr2/vjXizaZOkUtjY2uZ/G7eZAgpprlGkBbKsYEdSSh+5MdBaQhvSIrSNmoXAU54VAmMdhiRLkSYavuc5E+OzSUlTjULnkEpgPJpCZyleeunVs0LIo57nbVltcX+uQysAdu8xv7m58ZtXr179vo3NbWQ6YKEIfB+FNpBSsUBo0u0AsiKHER6MLvgEQ/jQfLoF8jxnDeB7hq9hjITWBe27FQJtrNovSDtIeJ6CUhIQkt9L5wWQA4UGkmkOXdVQ0qspRffwgQDc0WVPvji3tb33W2trax/e2NhAkhTw4og3SvoSwgBZXvCmkbonIdCQmJKtzzVvnIZV8SQkOWmGIkeaFmRS+ITTxtFpt1pA8wlXpOjpulLACMV/k4OZZwWmyRiCHE2yEUKiPdeKmo06/wz3qRY4dAJgT6f56M5u93M3V3fPbmwOsLtTIIpi6JQFA8YUrPaFEsjyDKPhCFmWkLfgTrbmTeW/yWSQQBmDwhhk8CE02f0ASggUhYBBwf+Whs5+ASMyFKkPw76EhFDgfAOZCI40jMQ0nUJ56l/7+Cc//MeNet2apvtwec7O3rPNBfZPypsfGvG965ub//srr15cXltbRX8whtZ2o+FJ/nXS7rkuoIXBZDJBRqefNji3Kt2GhHR6DXJTOBMh2f4HorDqXlshIuHwpIInyZfI2VwY6SPwlL1mmsGwLingk+mh99I5ut0uXn311Z/82EefeKVeq/5DMjH34/LSaXJPbptOyDTJoDyfHywl0GijSkeMVpoln11f3/qta9duNK9cu44+e94epE/OXQZk+947ncYky5Hm5PV7gPTBF4UAnUVjfUV2/OiL38GQjkitx2dIW9ifk1OolYGgU6wiCC+G8BRfSSBjgRFGsNkRnASSMCbHxYsX8ZWnn/7Fz3zPpy5pY/5vcx9qgXsiAHTyKTzr9kfwowqHZZ4CfE9xmpc2tCj0d61vrv8f167fbK6ub2E6zdhpo5NXuBOujWZ7S5uWJAlyA/h+iMJottFKSD7Rwn3xe1NYp5QzCQUpeNhtov9X/Hq+dkG2XUGQCUkz5HnG8iRQkPSyFSHfIAhIIH3Uaw02RV/9q78KFxc6v/rE4499SSi1eb9FBPfEBJQbYkMrsuE2HCt/prX+2Ob2zm+9fvFqZ2t7C93uAGmWI8sNC4CUnrXhOrcnGwZCGvhQvEF0KqU0iDwJJezrrGbRKISNEOh9yINPiwAp/4j+nzbfg1AkAOQfGATQ8LS2gsK/q522sp+lUmmhWq0hDn0EgUQYCLz00ktHz50982/OtVr/oLjPtMD77gQKIR7f3Nz87ctXri2vb2xhMBhimuTk4XFChtQ3LbLnmmN91ut8sn3SIr4i6456rYaFehWhZ7UNmRzKA5DHT4JW8CkWmIoQvcEEg+EUo3GKQmccIZBeEIqcQwNFNr88FxKccyATkGvKOE5RqzURxREa9So8r2A/5MaNGz9Vq1b/qTEYvq8P9B2u900AhN38B3u93u9sbKwf39zcwaCfIEkL2LBaQiqJwPf4e480gMiRJ2TDc/4+9gTm6hXUKxEWFxbx4LElzLer1va7SIBMDDloVPihsLE7zNmx3N7pYX1rD5NpjlFqHU0VSCDPkGcJC1uuDbICyFIJ4flkDNiP2dvrQpgMcRSgVq2gXq9gPBqdy7Pix6Oo8k/uJzPwvggAWR0hxOnJZPy7G5ubZ7e2tjEajqELzeYhTa3NpweZZhmCMES90UAmCkzkCIEwaFcjzFUqaNVCHFns4NzJUzi91MBCu8oRACVyfD+ApyT7HNMkwXQ8xqA7xNZWFxube9jYmcNwnGC330NuNLzA51BxmmtOIw+TDOO8wGCaYJoZFFkGSjGMRiMoZAhDDwIJosjDeDKmyODnl5fj3zHm/kkP33MBYJ/DiGObG9u/vbW1dWFnexvb29sYpQrTwiBJM5fCtQkXSd6iUhhOJih0gkgJnFzo4PhiE4uVGEc6TZw8soKlzhIaNSAK6LSSoxYC0uNMIPkDkbQZu84JifbOLlo317GwsYtkOkK/1+Uw0w8D1jhUX8i0xmBSYJLl2O4Psd0d4ObGFq6vriKr1jFKDUbkiO7lSDSQaYNWs3lyZbn180rm/8n9khe4pwIgWPFjaXN76/+6cvnKh1ZvrLL99H0PuZaYZjkLAIzN1dOrKUb3w5BjOKFzVDyJuYqPU502Tiy0sDzXxOJcG41qCOXnXNXjbAOlb8lzVz6kFwJByEIhgwCdVgf1uQW0GlexefMm6pHPzl4Yh/CVgE9uppKYpJoFICsWsdsfYPNIG8+HwFdubMKvhCjyHKoScd1hdXUdOk/RblR+8viJY58TInz6Xj7bd7vunQAIdt/mtre2fuvll1/+xMbGFoajKadx/UIiQ8beu/XxMSv6BATYoJx8kaEe+liea+PYQgfL820sL86xDxDGAXv7LF7CCo7hoN1mC2XgQ8QREMSg+JNi/ChcxjJFIEWOrfU1+FKh3qjBIwWlC84veEgRSolIeojjGEeWFnD65HHUv/kavvTMMxwOIvChKEGUZRj2J9jc2qvMdZb+g1qt8vT9oAXumQAoKerdvZ3/8/WLVz517fp1DMcppIqQQ2I0SiE9wXF+GaNTds/o3J5ID4gEUNMaK40azh1dwZGFNlrVCL4vYXQGzVk/2j0bapLnTqeYagGSwlCXgSStYNMFCmqugZVTJ7lGMO712AfRJCAUdoLjTVBFQedTVMKQfYrl+SZOnHkAC5UIT33hCxBRiKjRQOHHCP0YmvSH8CQJlr4PnEGvwF2WUkoCpRkGvf4/uHL16ve/fukiBuMEGWXiigyZlly1kzplcAbn5unk+womLzAdD9GpV3B8oYMlX6AVeah5AhVls3L0Ozn55xQmFh4kaQDK2lECn30AyRg+FAYyy2AoYej5YG9OevDaTSwdO4a1osC4P+BEEwkI6REqKqVJAkmhYaAQeR4iZVCPffzI3/gsfAH82dNfhe8FmD92HLVGE14QIyvkbm+cHcx6H9rl3c17JGVO2bkkSX7ihZde+/deeOU1DEYTpFRWpTMpNZ9Qtu+mcAJjnxsBNCIh0K5XsdisY6ldx7FqhErsI/JIeApW1dCK1Typ27IoZw+eTT4p6bKCruiDLIAhP4F/15qJuNXAEXMMva0dzkNQ4YeVCYEKgxCBJ9lBDKMIQRhAFRmqvo+/9ulP4drNNdzsDpCMRwiDFUjl0VcPt5Y9Dq8AkMd7txYlYabTyWOvvHbxHz73ykVs94ZQPtlr7TbKJnSMyDnrxkUXqucLyf/vBRL1agWtSohmHCCmmLtaIQCndSi5qEPPmc6rB2nKih6sFtACuhCM3JX0niR04xSaBYUqSjmBCOB5PqLOPKJKFe3RGKP+EJPpBJNJwNcKIp9j/jCweARMMy47txpNPPn4Y1j/yy9iNOyxE5inCfIi6Un+DPeBCbhbqWA+fUpWV1dv/trzL73Y3qTCjhch5Rq+w9xxdi6DILUtbY6efXiGaRG+L0K9UkU18ODTSVYeKtUKwihgNU0AT95YruTZnL7gegFrd87f0uZTRpdeQcUaxeVesBYoS8o6zxgORtcPag0EQYwWlY6TCdcP6JaUbzEEjEdQU8igwPYowbnz59B56WVc39rCzs4mHnr4PIps2svvk1wQ4V7u/FVJ42qDK1eu/cIzz3zt41zcyQIOmzxhizmS8/nO/yBb7qBdcOGiH/ioxiEUHfTChXYSmOQZgomBjENAFdahKwxXikszohgHULCap+IO6Ofs/ZFgeqx1WPAp9SsDews29LBmge6AspCVii0C6PL+DIRnwSiBB1RFijmlcPr0cbx29SJ6/T3GCYRx1Bf3iQ3wXGx+RxfZ/eFo+KPf+ObrP/P8i2vIVYVVKT1M8vRtjE7qmrz0shpX1vEFFOUByA4XER/lVAQYpgJBNkGQKURBjDDLGRegjeRTTM5bYXKEoYSheoBOoQoBqhSD/QWqCfhcY+CN0WR2DOf+KQIRXE6mm7AAEvLihQg5CWVKyBfZGxKqIIIpUtSosKUTfOKhFTz7VYFhfwdjwpWG9S6bs/vBB6C05h2/qKewsbH+o5cuXRb6AOhSMkzL7sEMteN+x4J2jMXy0f4oifFkgt3dHJEUqMUhkkQgCzWKUCNTOZ/GnCKJfMr4PRuyFRw9GK/caBI8xepDmxzKT8jDJDWNJJmypqBQUjpzQZVH0hqS70ez+Si7fywSqWB1QVqE3oEyh8sLi3jg9Fm8trYDRaFDlvYE3hLxcqiWR0WSOy8AHrZ3di7RtSl25j0VdpOVtNU5BnMeSJRwpE4PnBxz2/ABpQTCSszfD4cjhJKibA+hMPBMCC/2uKSf51NoX8DzIkj6AUV4DOCgBJK7urSaqUiHyKZTFHmGdDrFZDS2gqOtmVF+xJiFarXCn4P9FXIzbQHDCm5ROGAIEAcxGjWJJx99AhleRTOKdFWKLvs49xBt9W6X99Sf/+UdvyhtXjJNXqNHRyGVJtW738ljIdmeZ5G6RbEfDTDSV3L0QAUg8s5ZKxiNaZpiNBQQOQlBwfF4FFQQK2umLW6ANt2Dpg4gYcPCNKeK3ojLynSSd8Z7GPb6trCTZZiOJxB5wZpEwuYPonoNy5051KqR/TzOmlPWkiqEku/TmizyVaTyceLYcVzf2CHhTCMlh/cLLsDr9++8BoB9aK8bY0xR5MIoG2qWJ74s03Kd3kG6+dQ6TUCmglA8JCS0+aPJGCqOMZ0IBFJwdY+1CCOFNIUytnybGXYuKZ9IWcS97i76wxF6/SFG4xHnHrayFDvbO5iMx8jSlMNGJT1Evs+4gjjy0WlUkU8TLMw1EQcEYSu4OYRgaWNGKknGKlDPAKWBw3oFcbWJpcUVTIfj8dqVK8V9Uww6derU3bmyMa+mhZhkRb8yLSSXWctlgZV6pgnoe0bjU8qWsnNKccRALVu+UNCpgTQatWoVflBBEPpWAAyZW7IvqdUmnuIkE9X9x9MU65vbWN/YxO5elzuGR0mKPSO5L5AEgKIAjkZzjUajwUgfyjccbdY4NPVIozQqjDje3u0iSTWG0wxbgwGSZMIo5DCO0J6fx/GTZ1BvzyMvtBHS0+Rc3g/LO378+N26zY3+KNnc601PEab/9pCITIPnMPkMEiUbTBGAsOBLsrtJknKIVw19+F4NlVqIVqtOzZvsaJImSDNyzBSr8SSdIMnG2NrZwebWFja2uhgMR0gIL+j7yLVGTCe9XkcfBrvdHqZZBi8IsdcfYEC+QbOFqhei1hva9+XwhdDHKbb3htju9XGTXjsacdmaUs3UXXxydQunz5xGu9UOa51O5ODth14EvDNnztzxi7oPL2+sbgaCU73FG15Tqn56SOwbkEbg1K0t3jjXi8OyKI7R6rTRnqug3qgiigKLKaRuoFRznE+YvmEywnavh7W1dQzHE4wmKUcArYV5dBYWUa/VGUySFxq94QgvvPIybq5vIGfPM+d7mKQp9qYF4t4Y7WqIpU4TlUoIgy5WN7awsdvDejJBfzhmgSIBoijx6sY2Xr92Aw9feKgxf2TlbzbbjV+ambdDvDyS4rshANTWlaVpM3PwKisIuGXTyxNiVb9yhRh219lEBJJOf4hmJWbV3KgGiCOJMpVEDhtdYpJklHLGxu4Ie70hxmONQFXRXl7CXLuFWr2BVquNKIooTGOoGXX9HFtYxDNfexaXr18niDK8ILCRRrqDwEhUoxDtuQ4WFueRaQ/ffOk1DEddjLSHaQ7GElJiiXyR8STF9dV17A6G8OPwp7/v+z/9PwHYOexawKMO2Du9nAAcHY1HUZolEMp3Yd6tD2PmFNJZNxaFb1s4rIcdBT5qYYR2FKMREPuHZNCnpyzKx5PWt5gMh9gZ7KHXp+SRh1ajg1azTggdNOt1xGHI+GGPiz8eO38EHJmr1fHgqdPc/LG918V0nKLdCXFquYGlxSbmG1U0KjUEfgUPPvoI9no9XLp6EVJL1IIAlUYDzVodnjMve/0u+uMxvvGNrx8/enzp8TiOn6Is5mFe3t1QUw4GfiTPc0WbzFg7jVkjZmkfZwKhS4g4+HRSUoc8e7LzYRRyESiKK6hEIYeIHid2DJeZSXi0q+xRE4fnCcSxj2qVWD8IITyF1hmyJMFkNAL1QXS7FgNIcLPRNIGnDBq1CFVtsNBp4viRo1heXkGz1kRBLWGUg/Ca+Pgnvg3Xrt/AH/3FX6DZaKFeiXH8+DI6nQ5rj53uHp578Tlsdbdw7er1E1EYsqN6qAWAPO47vSiMmyTTY+PR2Hr7DOd2RtutUhB4iTJPz445J4A8yshJW9sl4oY4iuB7tt5rGAeg2e7b1LJGFIVo1K3fQAWiXncTe9u2x5/BRVpjd3eXq3/sXCqJMIpRbdTRrlWw0Gqgs7CAlYU24w1alTqatQYIdI4sRz4YwqvV8dnPfB+62xt47pvfwJy/gjAboalabJoWTyyhrjJ89dIl9Hu9ShoEh94R9E4cOXbnL+r7uHbj+hIRNynu5DG4vTZysAq5T/FikzXWF9CcnKEOHdvgqTnUk2pfcKg9i0NIQgtVq6jX61yyJY0yHIzQ7fUwHme84fTW5ECazKApredOUUiz0cDC4gKqtQbiuIJWowKfqoZSIfRsoYh8RDJRJsnQ7HTw1z771zHsDzAaDHDxtUvodft8jcWjR7C4sISHlY+e8iuelG98OIdseQnZwzu8iI9HCLFJdXtqqEwo7Ytby05lHoAfMHn+lNFTzvs3GXvqqkghZLX8Db5CGTpyLl5a7UUmg4Cj1UrErCEEykg7ObJsBcPRxP0+mRaP8waU8aN7i8KAG0tajQY7iGU5mVAypMWgXUqaO1Q0GD9QeFg5fgY/8EM/gme+/BXsbG1hbWMbF6+vYe76Gk6dOYvWyiLq1aiihDj09UDv2uqNO35Rd0JfrsQVI6UUkosrVn2XNYCyXcwyezBmkDe/cFg6qv9zHz+XlnNODFHrN22QEoo1BW0iWOBytueeK93mOmVhISBnozHHUK0wjC2hhBFcBCITEnDjiY+Ac8nUX5gjSxMUjFS03qhxZeCylkHmzAsjnHvwQe5Aev6bz8GLInT7Pezs7mA4SXCyyHDqwvmKcG3ph3l55UO8CwLwKgSl8NNYxTXX57f//7c0cAqr9ilfwDUUWJgYEzYo6Rwp46IDxTUCasmIgpAdTJISEgDaHLtxHnw/RkBOoxei2ugwEsk2+XkIkgRFltvNJB+I0UEETLFo4Ol0xGqfys5UzKLiD4FMmXCCqofKYpAeOH0SsZK4cfMGNra3MN9voT8aYDLqkcaKmo3moecN8Iy58xUrhnMpdXNlZWX9hZdeP50mBMPyynYwB9/e3/yDi/0BZvAwlqbFY/SnVcVCwVfll8e+BtcUGMkLDvV8eh/lQXmE34u4bVyUQEMqSikfXsXnBlAQpYyjfgHzBgCDQR+DUR9xEDFGgcpBygsw6u4i02ChigP+fJgMR5hvhVjsnEdvsMIdRtdvXsNekiHw/EcajYbUh7wq5LWbc3flwr7vTc+fPfvKs888e3qjO4EOA1TyApJy9siRBRIZ7z1hknw+ZR73+bs+fKfCaaM5GCDyhyKENB4DSTzabtevz6VfQRyBPtcSJAuA4msp3xJDcPGesWE+J30YEJ8TM5QtAxfTHNPBENlwgmI8YVNB3UHJdMj9gOtbW5w9pDxCqAKEoUJWTBkv+PAjj2BpZQnzCyvoNFbwypXLqML/rnal/QQgnz3MyCCv2arfnQt7nmkOGpercQzVn3LHLWXh9HTEFTedTpGlBmEQM2sHQcV4s0i908l30Gxy+mgzyZPPCFZGrduFRFKkCEgMcslqnyJE7RA7TOzEPEE5ZwtzsumMFVTQUkNqnwGj5C8U6RTFcIR8OMbO1ga2Nlc5uTQcDrl+sNPrYafXx+rGNvrDCTuScRTDI0xhnqBZp/a0Bcw32+yUzs/NoZACG8NBaArz71SatZ9h9PIhXR55wndJABBX4nUmXdA5RDpFTXl44PxZHD+2iKAR4+KN63j5lZcRGIW4Os8oHmrmEOz8UTNmaqlhCMdH7WNUGh6NUZDqJ6yAsGBQYhAJ4MMX9rRLoRgextCzgsJLKwhgJrGSJMreJyF5KdFTTCYM7d7Y3MQoTTjmGEym2Nzp4sbmLtcYNDw059qoVRJqE4acTnFsvoHR9gDilLEge5Ngfr6OqdCYjIc/7Fej/w7arB9aAbhx4/pduTCDMdJ8I66EEMjR9AM8fPwYzi8vohIA8/MtfNeHH8Hrr5/DX33pSxioADqqYJImmBa2v994Fh2ccVduBVOy10zxYjt+JwmxfKXwQoUwDzmPT+qZsQUyd3CzsSWYcL6JVCUQ1WYiCRhSEDQsK1Crx2i1m3j1uecwSXPs9MfYHkyxM5jA+BE7oN2ssExjuUadBFfVYRKD8W4XUT0ClOF8BdUREp2vFFnxESHl5+/KQ74DyyOChLuxyPXxfLVZqRC1W4pG3MDR+Qa86Ri76xvYuvEqtm8u49wDDyD+8OP45vUN7NGD1cqmbilUdBw+SZahPxzC0x6yoGAtTyROhN/MA2L0sASRuWeQewW/3oUTFlvg8vGSCSA0XIWXE0wUDlL6gRxGKhiFrQZudLt4/pWLKLwImTQIWjFUENqnJARST8DXPubnl3HuwQvscHZ3e2iqHIZ8AwaZRFB0/1n2YeF5nz+s4aDXbNbu2sWVUpv1eiUjp32x3UCFyJ56XSpAMNhi9eoVxuetLC5hudWA7va5EZTi/pGxbFxE3hxRAicKMKXaP/H75kDsUyTgcz6AGUCLjNHAOQUNFOYxqhfMA8jSKGyugXNH2uYeJNfyQ3hRBUElhgl9REEDjz3+JEzYwPWtHrA7wCizJJOEISR7PiG/gdrGBwOkfgC/1cbOaICpSFFpRGw+UplCewF0UfmE9OWhhYh7jcbdcQJhBWCr2awlQObX6hHqtQr6m1sI/AAqppbsiPsE1e4AXuChFYUIjAWQZv0ME9pIInYyYCxe7pC4CaF68xyxZ1lCywLSVBALWWI3nQwApXSVQKBs4oiiBhUq3vzACxl3GIQxwmqNm0LHApgMRhgOJgwTD+Mami0fMTGRwWA67CGdToBJxgQS28MBnvrq0xjlj2N5ro5xz6A6Gdtu5cCDIZBqrfaQF0U1A3MoqWM8aom+W0spb7U119pVnqhlOsPxkyeQ+REZdVTaDdzs7uLSjWtsZ5tVnylf2L6rCqbs7VvGz8KZgRE7c7b9m1TqlJtJC1dEEozOkX7AbVzKtXl7mrKNilO+FFqSqQgo0qAeBWnJJ8aEE9wdIKGewUmKa1ev4tKNTYyMj0lh0QdxrBBRk6gsIGtV9Emb+CE2kzH++Te+hiceOIMTrQYKBTSaFRQRNZdPYaJqUwbhgjH6cApAchfwALOLe96w1Wz+nq+Cn93b7KPRbGJlcQGC8HR0Yi+nuHwd2NjdRRzM4ejSEvpJAZXlnPej7mEijEjTKRJS7drAo4hAas4WkmMvCmvXC+06fShfP7XMn4IZwyzhI2XxKKevjE0K0U+meYa9nQn2BhNcI46iJIeXJehtbGK7N4JWAQzxE/g+IsTwKGLyJertDvaGQ1xd2+BsZJLnuHJtDXqUIm+0oBX1EyRIqAtqMh746XTnsA6Z8G7eWL1rF6diT6US/cby4pGfGq9N/J2dLk6dW0EUkKrtYbpzA9moi2yaYTihpEoDDS9F1u0xACSvVLBXDFCkCTJPoeJHTPOimQbAEjmzUXe8wHzGC2L0MNxYIj3LG0iZgoI1h0BQ2GZUooAdT6ZY2+7hys1d3NwaYjghSFpCWSGGeYXU7W9yREbCLwo2G2HURCOqoR7VYKYF5wlSeOj1p7iW9ZGjATR8hGaETCrIJNmr5GnfHFJC6btSCygXhVlz7c4zD5w7+8UvvP4Xn+ru7SIvljnZE1Uq1kmTAnONCsO8+t1dtObm7BQWE9UAACAASURBVKwXTzGGrz+dcI9+7ppKyWsvLLenFQKYWcGGgCG2/m67jiSHkgWHpHSKmZNQ2siAgCA3N7dx+eY6Vjf72OsnMCJ2/0/vHyJhFJOHgpqJNPEHj+GpIba8bQhfYpRmKDyqQ3gYTSYYdDcwkQaVusFS00NicgSTyY3djS0cWgEgxs27uSg7d+TI8j/zlPnUxSuv4cnzx1BtV9BcWMAjjzzMnvL6bh/D0Qir169wFpDYOPrJhIWg7BOw1USwU0hMIJRtY0Y/U4JQbReYnR8gHP27bQmXkhg+HTew8lAIgd50ghvr67ixvoHRVNtEkUhtFxNs6KipYEQdyFQDIE2TW9oaI8ZIdQ5FGcEo5C9KeI0wwereBua2fTSjRWSSOI7za5Px5NACQ7x8dOfxAAdXLlIcWz7yu2fPn3n1uRdePP/Rxx6CyFsMvjhz/jyqzSbn2K9cu4EXXn4NN65dwdziosUIUgOItuEaPWDNpWGBQhjm9bNtW5j1HgruPJauh8+4f1uWcNq7wvH9UohIWb7t4dCiejmDXHBVUBEJdapRiWtotBuIPZ8yyuhu79qKpBRIcoH+bg85elBxhEqzhqBe5SaS4WSMvd4eRp0GBNk66V1ThAw6rBqgu7V7d9+BQi/fGx47cfyXr1+5+M9ev77KQMqXL17BysIc6vUaj3U7efwY5uc6ePa555nPD2HMuXUiiZpk5AjmCJXHDOFU7DHM/ikswYPv/ACHLZCMMFZWEBhVJpjJixjJVOZxqnc8zeH5MapVgyAIbXWRGj2UgS80KmEV8+0OWvUWp5m7O3sYdnvcREJtDoPhEON0irQgLsEEmyQggY+mA6BSNJIRyFXjWkFd0IfVCaQPcbeXyAQWjqx8rr288jNf/No3nzx39jxq9Rb2xlNL9MAjWSRWFhexc3QFl1fXmdgRfmh775IEpjCMvKWiEQkBkUPn9GCVwxpKSwcjHZm0bcyxZeWMsoR6gkkiGIJOad5JRl3BEVrtEJ3WHI4uLCCirOGkjzwZ84QxCkAp9h+ltiO4UosQR9Q0WmetRM7keDrFcDrG5dVruL65xhzDYeBz6diLQwKYrFrf5H3e6bdYXlitvqtffMdv5HnjRz70kV966o/+5HN/8sWn8UPf92ksN2Lm/Sc/1Cd8f54yhLsWx0hoNk9RzOb3lDgB8unppFPopRy/EOMFyCRwMch9b9lIHRrZcY8Rk0ih2RwkkxST0ZQ5CqthhCML82jGEZJxA+ub6+h1h0xyMZiMsNcdYDScsKNXqdQYLUxDKj3PNrxS0uHE6ZMIWhXsDLtcLSQmFF96Sa7Ry6Z318y+l+XV7xIe4M3WQw/P/06vP/rys1/88ic9nePjjzyA450qmlWFWlDlU+cxLZya9f5J3nKr0gm/GCmfvX/jBgBo10dAK2d4uMOaOCGwWELjaOQBScxfpMaThH0G6iQmvoCUmkU1kUP5iGod9EYFur0hktRgaBSGRmB3kiLtbyLAJiKKZCIP1UoFtWYNi60lLJ88BtmvsKkgZFEMM1SeNzzMyGCvs7h8797MU9lHPvmJ//aF1179/adeeBGvra/hQw+dxZmjK4j1FhdnxuMpBuOU8ZdME2sK1wmcWaqWADzRg5M8BOAk4ShspzCzhCrpxsrY/+d2MzeUgsCqxEg6yTXGmUFiBGcEx0WK3ngIhSoKs0fJQVSqOfpjwizkyKTGkEbTwUeufOzqHkw+RkvV0BAS40wgzKdYrjZQHfkYa+KRSlEHBlKpwWEeJOExNeY9WmQfF1eWPv/4h5/44z/a3Pjetd0dRBc9RHENdSTchEEdweTwMY1faEmcqB+A7LglgHQzB0QZAdiN5soe9KzLuJwZVPDfrvRLgFEjuXuY5v8R5LwQwM7ODqpKouIT3Iveo0CrWYPyQ1QHKaqTgp06av4YTTJEnHm0aeiMZhNEPr+/yS3BhXSJKc9Tg6gSDw4zLtATd6Ex5FstQtmcOH78F6Io+LRIoajMSx28jUYMo3JG8ZDqJ4ePiB2JxCkkSlZ6oL7HTSKB9BwkXLipIpYBTDhUkHYcgeQYMgN5OR+Ic8C0cSlzBBELKXnnJAg0fZSmi1IOotVocyGIqo9bVA2cZliY72Bjexfj0QRatDixRCNoqfmlFVfYdyHsI3URk3BRsskPwmmtUZ8e5iZR7/WXX7ynb0hO03Q0furokZU/uPTKK3+jFoRMyy6iOQTM7uLByAyCtAAsMZMqO4Ycewg3mgqb+NGzhI/NElK0ANi2MkIaa+yPdBM8A5B5/LivIPRD9iLYmfMkO6GTRKJR2OkihFGkEDXf2mU+gHbDR4VIRhONSrWGlXYTNFuYcY3C44rlZDq1JWyLSbx4OxXOYVteHIX3/JYa9UU8+cRjv0YCMKbGzN4AyZEFxH4IpQU86iOg5g4q+1L+nrr+csNZQDrpGY2CMYLDLT5bxrjuIViVT6rZQb/UTMNZjH4uyA/IuauXVTXPH9ToT6ZMB0s63Hh9iN6Ycf+kdbq9LmP+iSvQcL3BwGQpapU2TN2gNxgzsaTnB3w9TZ1HUViElfgfD0fDQ90e5m1t3nu4Gqln31P/79Fjx59fvbnxaHc0xYS8ZmrftvyRbOu5FEx1Ac732IRQThQghPQRZMvLNLFtKbOTnXjMFP+tHTm0dOEiTyCiphKPMnOWeFIYK1jE7TtKuww5r/fGrKkI2kWNIgRJY4CJpBifWsytI+qnGuMkt5TyeY4k79vwNAjQ7sz9cVSNv4BDThbmUXz7fiyCjc8vLPz6tetr/31vOEZ3PMFcu8HxvPRcHl66aV2UXPE9jJMUaZbaZg7PjZArp4AJ6+zxFwd+1usXBBixE6r4ZxlFD1JiMsm4ZEvsIMQfSgH7hEgf2H53OedApV4aUWPxBpZaPvDsyLt+otFNLXkV9UCSuRknE2uiPGmEp36l2+/rw04S4d1c23xf3ljYCP0341rt56eZmd/Z28OJlUW3UYL/kKNH6lqzwya5KYNAnHChHTWGsHef2xNorYBwOQTLOcjOoePtJQdxLDWHdpNxhjQbw/NDdji5W8FkPIuQHVBqJq3WLW+kNiwAPnccaetEijGC3pCjFPJTCKGc5ylripWjR74plfoTchIP+/KadxES9i9aUsr17Z2dz21s7P5Hg/EEk7RAKGwzB+fvckcfx9yBijtyJtQHKFx2z02HdPPDbecRxEzds8rn37dlYhouPS4ynkpWGDIpmhHBkyzhci+9L0HPM64EZhgnBSLyS0QJI9PAmChhEigaUadSvrbi/sR0RjjZbLe/Vq1WU30/UMTE994HnC1SrRceOPG/TQZb//5wkoZDirdjhVBa5i/Aonup26fghlGa8EkdXQnNF+FePsCie6Ww3r90vMPldFKjgIQwBMLiBzzKJNFEcSMwVT4XdJLcpg+9XHKHEjl/hgs4GU8NI41CLWhTYgPPU+YuIByDdL2CnnU9MB4lqNerBDX7ap+5Ee6t8b+FdOP27w9wMR9c3oULD9/Tm3zDDXjes5PJ9KXnn3/1yUG/h3Z1jpvFmFdYWxZxagWzUx8Eq/2SsZPSudw5REUcXexjAVzyp9B2AgknbIocnhdwWZmAp+PhBDrTjqzSahXWFjxS3tLKUjay5DOa0s8djT2NmTWutkBoJTJRVIgi4ejMzw/ac/N/eJAD6W6vMulFz4KIMuzIXI2V5SU0GzVLv0+zl3yf0dgMq3bPySO0TdmoSRc42LV7j24+P3HyxOdff/3ak1Su5dg9LxwTp3HqXsxAH5ZC1oZ2Jeso+QNUApbSJgdosykSoHw8wb6MsgUkSvRoo5AWFk0sXAOLch+XnDzbxlUSUFg/wk03hSMeZu4CzkxSIom0iymQpVOeKLK4vPIbBuK1LL8XFUAr6EHgI65EePyxC5jvzCGlEJiKaMxlTE51BCLrJPr90JPcDTUTgCc++jHrWEHgxvWrGA3txAx5j9gt6CZq1dof+p73X0yTsbIbYFwXsXDsMW5iqBsHwvl9zg66a+hiRvbMVPPK54ZTeg0VkMZpyqPk0jTBZJohpRCQtAE5dm5gBpkRoo4tnNN48CAcpLOh014Yq3EkjaEh4SoIQFLBo49eGD544cKvqHtw+ssEE/EmPfHoeSwvLfK+kUabpcgpZ0Iia+wAbOZp1hZsOxOAxz/8ESTUDas1zjzwAPq7O/jnf/YUT9q0PL5W9ZKUHaR6u5PLU+prCwsLF7PR8Lx92LA9+8INiy4srbtHtHAHHu5sg/T+97b7x2Z9abDj3NwcTK9v6WYphSwIMaz5lLiRETxckvMFJDSeN6sdkKZhfgByIDNb0uUwzyjGH5IwkNYisqgnH3+8++EPP/nvNhqNVwpHPH2nF90tRTwk7EScdfLYCua5uVcf0FLvbHlEmUZflrJFodlq4gd/8Ad5Pv6Nm6t2ni6xPbz8CsbjMe4KoYRUw6NHV/5s/fr18+PJkEfFSGf7BftnjlnsNj6Bg4Ig3RdmChyzz9RqtQiVhNFwxEOfi0mKPEuRO9Ani7jQ8MiL55kRzpwcMDOgsdIzP0BjSmAWARxZmscTjz9686Hz5/9mEARP0QnEHe4D4qIW2XilsLi0iDNnT2Gu02EKnX53j/kKvXc5/emWgUGlzaNOGhrG/PFPfhJ+vcFqd2HpCHPibG1sWkfsDi4hC3Q687837nb/FhVmBOHvjaOMM5b4gUs+RUkp42jm3EmdjYBHCQ60nA8E7KS43jjiabpvyvQJStlOUuxRYYdazdx1NJfypCWtdI4mnXzadBJ8Hj9f5IDO+OQdP3F88vBDD/56u9P5ZT/wr+V3MOdf7gX93Wo1uWn15KlTqNYr/GxII0/7o/dsat50YlTpVZJN4T67PMeZc2dx6uxpPPX//SEuvvoqgiC6Y3LO9tYPXp7rzPX1tN8gjB913wLOEXQkY0zo8AabrO0AKAaAinJAJSws1Fb7iqIEhIiZaq/QOBg/wnCaIMlsAkcaO8WM6wl5NiO1pquRYLLNrdSwMtfBhQsP/vaxY8d+IQrDr1E2kAkp7sDi006HkCacduZw5MgSlpcXUW+1eT/I5MAVwe7Eetsjw1gYhMB3f/b7Uav6uH7pZQ6r7lTEoDw17u8W3fEkacAEfAqpKVCR7ec+P8nz/XOObV0ZmKaCERcA/a3L1I+eTR7VbtYPn3xiF/M87E1t0we1jNfCAFEQcJ8fTSfJMktHB51ysYnmARI2wVLRGSwvdHD27Mk/O3lk+RfDqPKH8iDd/XtY5YGje23Uapib7+DU6eM4fnSFS8wEmafnfzfSyu9oZhyfBiHwse/6DDN2b9+8YsHXdmLDe1pKqVG3WR3koz6fRs/N/eeUMGw2sBCWPko7J482suC/hf23NRT71ZeSj4iEhkIjmhEYxRhNRtBZwlh/yvIRyxcRThD5RJJNoDwBRvIyJ1DMb9ZqVS498sjDf7dWi37L82RR5MV7Vr8lSomEMK7XcfbUCRw5soJKs4nxeMRMqOzV38WI4h0PDSQhUMbg4Y99J9ZXTmDt5a/bAUtS4r0MoJJKjeZa7f6k24PvboyhHZZRioF/di6gbR8mmcu5b8DhAUyJC9g3D2W+4GBGrEIj33WIlMbEuawhNYWQAxjVq0hyZWcBCAVJpBUajExeWOo8JU3xm2yXffmuP2l5H7SxpOZPHz+KYyvL6MzNcaLGVh4Ld9rvrK/1ZutdTY20BZgMldYcjj/6Uay9+CzbzDJkfDeLzrAnZI86d31m8CrcRu4jfK23bx++LfMaLsLwTyhSkHYKmXH5gpKNrPQZrKYqUCFuQOoxYO/aElvSz8mvMVEVk8ACUSkEJEGhkGuh1TiqTA4xIZp4HyqsOscTzui89bPKneqme6AhU/Rpz5w6ibOnT3Hmjuw63cvdPu1vtt7T2FByDivtOZz95Kcx2lrH9a8/zRRtQsp3XgSXBO02E1lWCrXrABaOSdTYFi127IQdAOVzzM+/xzabR9A513RG0swocTek0uZIOYfvM2egsvUk5QhFeESYHVRFGT56XRwRX1GOii+OzM/P2+u64lQYWBq7TAmLXjqwSi++UolxamnRZhiFwdJKBw2ioG8uIJ1M2fd4PxFD73lurMXiKbRWjiGs1nH5mS8inQzt+DXxDlwDqSKT69MeJ2aUVefaQsMtuYNhriEe28YZQu3gYYZxA+SQpYUFhxrXM2ZPk0MDzsyAyzA6V4FTWyVZNVsXUcYPPE2MsmdULPal6Rw5ulwxxozdJ3cJKHq1hYPZS1kVTqXrpWMncPTkKcRhzBPOC0OgkykPvMhnav79XXdkcDDDsZSHxsISHvmezyIZDrF18VWMttcsQVM5EOKtxIEnfOszRZ4/ZDl6C3bOjA0EGO9vR7fZ2kDZAsaQceyPmFPO/GgHEbMhop09AGc+SOUzu5ixm2wcjb2lprB0M6RiaDiVpzSni1OqK6RpnGbJgjHm6pt+BOeYkg2nIdMf/cQTiOpt9iFSSrVzCjmfgVYOy7pzk6OdX0BawPNCFKfOorFyFNN+D5O9HaTjAchzphFruG1kPZ1iXRQfyZIkYjVd6BnEy9puZWv/dBqVzQvYBlE7A7icDaycoHH8PttWMzMD9rx6KC23xZZb/FDJWK4dqygVeiyZlHBp1qIySdJFY/QbBEA4oSPeIEqnnzl3CtVmDZMp1VTuviP3XtYdHx1Oto4EgQiViULFq9RQWVjEtD9ENh5i+8ZFx+iJMm/H6ebpoP/pZDRmW11CuLR2CRyuvQue6eM7kIfFeEqX9CnYgSrFSjpfwHYIlWggzGjreaCkaykDO5T2+pxIKsfNu9GvPJqObHxWRGmaLTFW8TZNRpqGBlo88aGPYvnICqQzA+/WIb6X667NjjeuWYO/Co240Ua11UF1bg79vS1MBnvWa9eC4l05HvU+IU3KCR466VRuJTeMGjkKacfNsgmgdDCFYgVR8uV202gGkJZ2syhfT1U+IXm6uC7TyG4z/OJWPgQt9CxCoFeSKbGzquzkUcWQb4F8PEUUeGfnFxZmtrtMF1OW8OwDD2H56DGuOFLTqIe7R7xxJ9ddE4Dblya0bBxj/ugppDTebdRjhk+TC/TG3b+zt7t7gW27tCrYYzygC/XcLF8ielKO949ZYWlWD43qzWyFTDFIxDZmlDMJs3Ju7VusW/APdqbNzEGEA5cS3oAAqd3dvUcIGVQKQHnyT5w4iXqjCYutuFdP9M6seyYAvHi6Z4JKvYnxaMDolGQ8+g/X1m78Qq/fR4WAHVbZ21MJm3QqMQG2S9jm8jkdXBAYQrGKp40nz5rOfKFtDt/MnDtwoUe4xpRbb+mNDtk+8YRD/lDyC6Cmz+8/c/p0h6aB0etIW60cP4HFpWX2Ae7HdW8FwEUMtVYHgxExf+b/diUOf6U938Jgc52HQEauM5jc/zINLBzRQxkvU+aOBCGgIf7GIoWUCm0/AYVx1P6dJJZD0HUQzQpKt6031DLM/ngb4/6PBCsIA/R2906Gyvuxo8eO/iNy+IjNVIaRG2Vzb1vs7tR6X4baGAvX/uGdbu+f9Ecj9dAjj+J7vv+zaHbmmWY10XZCR+BLVvtkBoRD9gpH/zZT3cKhhoUli4wiH4R0rtVo3JuyiR/m9ysbS/eHWGN/yKUrHrmIQZsZsLRMMxPqJxlN0Nva/cm4NVcR1SqG0/SewufuxrrnAmAMAuUHf2ducek3VBQGW70ebmxsEnwH3/6Z70FjcYEJIDNoZBRRuAyaBXxq+1VODuOegcJ26uqMOYZpo0loKsTdE1ct1wDlC6SFmN8ypcR9KYcXsI4gZlA0WFZZW1OgELbQWLtx45Htre3vGAyGOLwdf29/3VMBEDAf8oPwD1Sl9kv1xZXoocc/hAcffRJetYEr62vopRN826e+E4tHj3L3DgmABWkAudnPmtnNMRafV+QOnlWwZrGzhWzDJ/U90tAJ4gBSws4Vn3n8B/4uewiVkrMsoXBz7IRDBtFrIz/A3tYOvv7s1//H4XD0tz3PO6/uU9VfrnuE/NSUDvxPlRc8JYPwu+GIm2RYQefocTz2kY/i1PkHcPHqVWx19/DJ7/h2nL/wIGqtBt8h5QloYjgnbYTlh1QzZ864xNJ+mzjn/YXdNGLwICIqYcqZQ/INQmBcQyncxFJrWlw4SOTV7n0p00i2//kXX3jwS1/+8v9w8dKlr+91u3+qlPfLSnnfJ4S8d2wbd2jdXSfQplsfCmv1f1TI4HsFhVDSDoKihBENfuTNjVo4eeEJnvLx1a88jcu+h/MnTmDx6Ap219cwGQ3RHw64CZNrMXTSiaqLmEHywnXuWLWdGhfnG7hpI7ZIRLl5BrUcgJFhJj4Wd8xZP9iso+GikeQspK0tKL4OpbbjosCNl17C9Zdeilcvn/v0xz/5iU+fPf/Afx5Xm5t5YZ7RUM8IiD8wxjz9hrTnIVt3MRFkoAL/h+M4/hUZV47mNMunhGU5zh625YUtqtDJO/3Qowy5fu7Zv8LXXnwJy505zDdbmF9aYthWwj1YGkS6NOkPMRkMkY4T/jf9jDxxaue2c5rKmr0dPlVv1JnMIZ9mNsLgtK/kUNKWje0gahzAHHLN2WmJkquQ1E+Dgo8wxE5vD9deeRnDvW288uJJnH7g9OLSyvIPHD1x4gfCMPx7Quin8kL8rznMbwuaXPEvhwDwA4yDIPxvolr1PxM01o1AldJ36VdR1l5n9scSPihuuT57/gJo3Nq1i6/j2usXsb29h0rgo1KrQESG+fxoQCR1EC0fW0CeTDEdDnnGz+aNHeRjzWVjuDqd7ROwrl1OPYe5B6Pz2bxCT+wTTroKk51NrmzyyVISC/43bKaYU8e1RgMqDJjIOptMcfPyJQy7O3gtjnH02BE89OjD8tgDD3/G89RnYLKfKwx+qrAa4VCtOy0ABOL9dr8S/T1fVj7jjG5Z7ZsBO1A2fZQtOayCfcurE/pYPHUOC0tHcfrMg+jubKG/10WWTzHK+paTN0nhc3XPQBnN7U+LS20sLp/A3mYPa2urIGIGyj4ah9H3A4mqCBljaMzEziKaNYFIzPpOZhHCfqNpyTLm0kOuLUygGkWUHua+grl2k/sCaaZh0evh8je/jrQ3wOlHn0C1Wvtwkas/HEyKn9NZ+r8cJsKIty8A5gDWbtZvyPa0ISAuFNr8K37o/ZDw/EfI5S4zabNuHlnGVfoWfP9smjh746Ft6qTnHdfQOX0OnRMneWQrefnUbjUZD5FOx8iTCXp7m+jubePm3i6yzT0sV6nW0MH5hQXucOrv7aK3u8szfm2ISP6G4oiiGBfM+smb7YBMGqV2Mg5Otg/WlA5PwJXcnMcRMoUMCUk9DrGyMI/5Thvz83PsQ1Bv4XDYw6VvfBUnH30MXqvTbITNXxeT8GPK9z9vjOnDmB1jzK7RetcYk93e3Hkv1rcWgP1YuSJ8/68bY45BYFkoeUQKsVKv19rS81oG6myWZcJ4vi3ZauMYPoyDVheslkvP+/bB0TMZQ1nlA88LsrnbAAgDdshCESNs5LZrSOc4wmPhxxj2+1i9cQ1rly7j5sZNxszPNZo4cuo0Fo+sYNTrsSBQ8+kkA7xKyEWg4XDMad5yKCVvuZAzjsHSV5zB0dx4W6vBqN+wYF7DRr2KRr3CzGKsEZjCKEC71cTr12/i+We+gse+47ugWvNoNjs/bTR+mvhmlecP/KLoVbXuGq338ix7Vir1pwBeMNq8Xh6e9wUUSh84n04wNkDcaPxjL45/oiyWlCgKHyFP3GCSJPLGXRlXHHDCODXryrVwDai3x863Z9PMQSTRvpWAnDV+eI41yoOvArRrc2ivnMS5hx/HxtVLeO3ll3FlbQ1XV1fRqlTQbtRx5MQpvszuaIAiK9DYaXOTy+7WDqaTBDRdkOYBUE+CcOBS7ZzTMntYCjADSghZIjXieox2p4Vmu44opqml2jm4tmy8ON/B869dxt72NuYXj1EFix4CBM03DlCXUtYhxDEek18U3wkpf1bnRVd66gVPir/Ktf6iUuqSsdqiq7XuCiFKmNMbWsLvjAC4i+ZZFhuIny2mwU9U3OaRTZc8wVu7eb+2N08eyLDZyUxm5kGXyZay0YKRxU4IyuKMPNjXfmDq9uynTAw1nr2HLE2Qw/rRvYSVGk48/CEce+AhbN+8hquvvo61K5fx+pWriH2fk0K1hTqq9RjNTgsrx4+BuPy31rewt7OHZJqw3yBmWADXnOIKTlzaLv0DYT8HU9dVQviRz2RGhcpgCglPhFxDILLLpaUlnnTGASEPxaI2ddvvOJNwUWIUC4jAbwWh/+0wxbfHlfhvh2QC02ygjVmL4ni1KIp1AdyQSl1VSl0llJLWelNrvf5Om3rfIAAGqAql/o1qrfZtQsonjed/RHs2tCpc9+mEOXEEe+s0eMmWcMWBJMw+yIrVKayw8PRuXcxanmb9fsDM9sJl4lDi/2cqwDDerkT5ubTNAeh3YX1+ThR5WDx5FovLR5E88QQ2rl3DjcsXsb25yTMCKCtI3byB8BHWqjhyIqC+fu4dTAdDjId99PsDS0bleSjzQuUQfAYnydIRsoglyVRzyiGZPGhjuQRoAtrC0hIazTYs45XPTS404ayMROBArBa/omY0uKXzKWnYhR/UYQrSFufLPgdUYw5vC61NlqZ7qt//f8aj0T81Wv/F2xUAke6+zN3BPHxJyu/2ouCXgyj8uBdHVpVR4oSAFTSKJct4hu+wP8Tuzg6fvE57DpXlBiq1GpsCuvmcEzZ27j5P7TZ2oqb1pEutYcOp2Y28i5rK7arPdi8LBnEyORQdO/5cAibNsLe7i/WddexsbSMZDZFPE2STCQwNt84y7h6KVMBC3uvusUBT8mg6oh48N8KeupMSCwIJ/QAL8y1ceOhBrCwvMJGVNYOwXUwU2sZ1hO05xO15ePUWzWoFz78Uuc06zrIVb0ONH0hguR9QbtJ+dsP4dowHg3RnyxJv8gAADs5JREFUY/2/ztLsFznMJSCNF874AWhkvin2+QGsAIzGHS8M/ks/jn9W8URGYefyWNIVHsV6C8xbG6TDEdZurmJ9dRVezcfpc2fQanWYUpXSqgklayYJf8CoEiEmhkUbFzpE7e0C8M4l4HYBMGLfcCh3umhjKYpg3mF6X1Ego40ndq/RELubm1i7dh27Wxs8p0DmgjeWc/+hb0fUUM8DgT9Cyzk8GifMHkahZLtZw9kzZzDfbnH9QSkn4IGCCCtApQ6vWuN+Az+us1Nr+wmKWZ/D29r8N38C1hQTVsLxJbGJGY6wcfPm/zwdj3/O9+XgWwpA3n31R4wU/5VXiR/bx1DaSpx15SRz8tnOH+vk8VwVl5TfXr2Jy6+9whrk5KnTqDcbqM0vsGqkUKzX66PIU1RiH812B14QzwCZ+t0c+2+xiKnDEkjYJI5g1sZiv48MOXMMSe3CUz6KBca9AXY2N3D16lXs3NzAqDfkQpNt4rClZKaH96m2UEMQx+xP0GbXyclsN2k4Fr+OB1STKYxo4hiRS9MAogq8uAZFAmGslmQKTLnfsPruDkC5+diviZRCMKHS9eZT437/J6D86yB28zcTAJNdSyAlA9i0RVTYUMzBoaxNFqXAzTzxGZ279JHs7eLV55/D1vYOVo4dw8qRo2gtzvPAB12kSCYT7O3u8PWr9SaarRa3XR2U+31r+PbX7Q+tKNlFXGqJoxGj93EDjDPJ7TgaKiWnKf9/QEhlQzmGEXpbe7h56RpGowGyLEFGJz1N+HXUr0CdPGGthkq1ym3b1MwZxxEXqzhcZEIrah8LWMBz8g+CCGFU5c/sboLL3UK+dUj8tgTAOcDS9TXYH7oWFTu8GZPu3sXdnd2f0Mr7AvVFvkEAdHG9JyCIBMC1VM/uxv7lBKBM7JTtVvtUDIYBlOlojFe+/jVcX72J+aVFrKwsc1mXHhYzfqYpaGrYcDxBqz3HGDp6WAL7tYF3Cqh78/DHzBwoCyPQ1j7CkknmskxBW+1ARBHsJ3DCp4BPDhwNkyA+IW2JppOp7d6hlDIPoeCeAWI5j5k9jO2+UrPnQQ5zQqSUTDjsw48qiMKYtSkLAJlrFAeyju+Ol0nM+l72T7+YucklFg4Y97qDvZ3dHwuC8PdDmtB+UACK/BrFlU3MEjG3OibC7AsAbovRS8WgCpe4MTle/sbX8fyLz2Nuro3jx4/j5JkzCKo1V2wxmE6mlnImjHhMPA9t4JDSe8fNpW8pADPmEH3gNbZpzI6gcOGdy/RRbUCU8TSzi9rf4QhDOpNn9OzBciXT9QXO0sdSzdhDaN8LTnUr+FHEY/LK0FW4B5gbPUu04V36QLw3JQLbbX6JlZh9FbaAkYwGw2Gv96OeEL8Pvc8RJLL0CgsAx+sldt59sLIyNrP9rkZeAiBnOHxHrUY6hKT86uuv4htf+ypn5M6fPYNjp04joMkkboa/ZftU9sHMNIB6zxrARpEuX+/CyrI9rHQQpZazOxfMBWAsF1HZVcTAEpcJdBpAuowmJ3/Iu8/S2Zg6G3pKFmAipVKe7yaceBwakhA4pIJrN3P7Yg5o2XfpCxFsXThTYA5uekmgS/kXaoTR1lfIp6P+pN/7cZ0lv1/2S3i3OyBiZhu0632Ts5De6X+UfuusRYJw/EXG0zdI2k4+eJ6xeV/+8z/H1cuXuMd+pdq0DKCu04aIF8hZ4zgbsnw073kJN3y6TCmX6WUcMFrcGMIvlLPcgylPOE8idUUg7kaV7C+UYFGuLpZqW1FizHP5DMn/tuxlnk1UlPjyWcLnQHrLEW7t50zebSTgDqZrWtUzYRczVDMJJWh/orhRBX5juLv54xrm8/zbaf+VLoRsMr8+Z/pKHV9y74Pje/vk9pMys4KOFE4FFXZePqw2oJ9vXbmKr/zlX2BuYRHnLzyGuaVFyCBgHj/i7iG8fRiErDUM9RAaOXvIevbxSvMjZzFvud6gAcqWrwOQbjtE6OCp2dcUtt5QJnTcKRJyJhDGmJm/YGloxK2Pody8A9XEsk4wEy5ROtUOeuL8EmYpKWFnbxIKmrfRWCuYL6H8rKUA6H3nnTSAtOAbWQo4majxqD/s935MF+bzHiVBaL4NqQkajhwQZbvvcbpXUOOGNrOW6FurgLaExg0Z0tKz2wmM1tLSo1k4fQYfmqZ45itfgSiewwN4BM2lBagwYqq2IiVwhmUFtRU5C/M+eGLt27rT8oZHcttDQ7H/M3HwzB98sMa9xUFH1gJCxAGhKlvTy8/L94bS3t9+G6VJKV9hyaOtZnAhb7HPKGJc3dzKTllyfuP6Fv0sbukZjmFmSjX2P5mx2VOr0By3MjGlBEGjUq3+5mgw+lGC6bjWajs5i5A3ZLtJEHyihGPn5vbnyKgILpbkRcaS7wmHnqA8XJLYXrs8w9EHzmKaTvH0F7+MXAmcw0McHXjcqWuJl0jY7Kd1dVnXCVSGogIHbdxbC8DbXW8Zb7xV8/IbegduRXm9ISEFy2NUFmpmJeVZVVHaYha+1c28d8CmcXwIB1SWFcQ0gx9GjZoRn/OI9boaxIiKENNkyo2XNmymIUg5m7Pb5wpJV7iwjFZ2aHNRkjTSuHXPw3Q4sgOUdIazjz6C0WCEl197hQEflAeI6w3kSjI7Rqi1ZR0z0gFIys3efzr7+uDWR72/3p4jNUMhvemlDr7vgSvPvhXY5wW47UEfWPpAEwvc8/RdxFOCVW8vdok3+/5fIN+3B0HltfWMI0E7rkRxIDJyxFapoSFYda8EPoALVYrTnnCwaHnwXYw5oPusCygde4cWTspz69hxZa5axXQytrP+AoFHP/IhHqiwtrqKlaNHcYoaNzwf/W6Pky7znUXm7i/t1OwhiDKt82ZPxLzFv996zbwLcWDTS10rxK1x9C01afNmsvGmoagjH3GaQ+KNTr65Tb2bN00H25DR3MJ7dPCjizcVHzP7jPs9FAcEihpnw8DWNWi/NFOykgMrmAOfZHN3m2BYe7ABbW7TqfQ3uRjEqZckDhMg3TQNxVg9n7JeoiRy0vxGNPuHnD7KxT/2xOM8XWt3cwsZUbd6ErV6hQkgeoTqmY5dnc9y9tiCgbb+xlvM3i2TKDP00Zs6h+LAz13ShMe5FmymiDGsSKf8Zehzlu9nXA3XFLf83u1H72Ayp4STlRNsRPk75e+763GzCg/GJKKrnN+XeZbK581d1QR4mVD/JFJKTFH4XN5fSYYjSnVm36eg/clS2HFZjjG9TH3PfA3BWpvK1Z4W2oEdbUeNkjGnRLc21jAZ9DE3P48grhDCl4sq00Ef21vb/Dv0f9Vq1UYPtn/bgUFsFw5zgjD7dwadTlGtxHjg3AN4+aWXcPrkKbTjCNVajTt4R3t9LqbUCOvhBVaqpcL++Rdv0oB54PTCMX5hP7lS2uCyRm5PwX6rF8fH2sb4fCKIRdyiWA60jNulXOcQL05a4a3TuOUQq4PLOYFsg/kSFiVtXMhZRgSlpinZ0aSxbGhErkEH0lZYfWcq998P5ZxEx4KOAz0Q1gcB5yXcjH2+Nr3WK0EaytXhqVY+12zxzJ6tzQ1sbW4xyTLF64uLi+gsL6MejXH92jVsXl9lHl7ira02Gghp+giVf9mMlFArYXkA2L8QmGs1+MOur95E+8gyn4ZqvcbI2tFwwKey0WpzkgiOur2UW3ObGrxdLWpHLvFmiRXjZgFYRLBN6mjHXaBnjKCWXpbmCJQIIDhKN06aMB9/RB7vPrytVLwHhJPp5xzTqHFgFc+litnE2sDaTjm9pcGl/KcL6ZyWokQURWJ2kIXVjlycc1/uw9v9KzuZDuRCys8Px/NIiCRJU1Qzzt3ImZfKMaprra5XqzBz89jZ3sWw12eU7dr1G3jowgUcOXkSlSjGyy+8gJtXr2FnewdxpQJi0ZpfWUZAA6mdhMpSAJQFjdBDo4dx9fIVnHnoIYTNJjyh0ZprYnttA71ul5k9aS6f6w22p5JV67dqw9qHR81w/QcQR7yxlPLN0/1wT5sZiUXpOHFY62YRlYJE+X641DAJgTnw8GfmpzyJZUDmBlLmuS37FsRVrBQLE6OjCFtRWDDJLWkg9xlIeJI8c+Vlex/qwIk2riIrpbx1fkJ54l2nU2Gc006ZQG6i2U/00/eeR8UPqWdsn/TLAUu5h6qn+GQjSZnOjEasrq/d5MkYhHJ55LFHcO3yFWxsrWNzbQfrN69iYXUeJ06e5EJJXKlCVio0Rcvi4IgppOKj3qpibW0T/d4AC/UOp019P8RcB9ja2sZoMEUQVHhOn5DlBxD7zSQHPNrSeRLCjpjVrjeQfA7y1svNVmxrqWdA3/L7lJji/H1uiaC1y0rSCeJ+QU8x1PvgSeU8Agn0rF1dzmoQxtliO2SKDldq6W4yel+fR+CRnadoy0E5Zu1o+6bLJos86XwJua/tSt/CuGSWcY2xtm6hrE8hbKFLO7PAaKwDyoXIsiWjvApmTLKoHftJbIKCxrY7nnyassnCUK3yF4WKw/4Ar7z4ItrtNuY6bTRaNfR6PVy7epW/1tfXuT273W5haWkZjYV5hPU6j20Na3XMdzrY3esymnc+Zwwcvyf5Gp3FRSTTqaVHL4dAlALw/7d1LSsIw0AwrdqK0Iv1if//V+KlYvHai4IQZWZ30rR4E2qf7E4mk83sWGXuUDvFAGYHiA+dS/28SXZm5Wcib6W5ECDIoIbC7g3OJMx2GFZoQUjXwnFkMdBi4YUyvtFEKIKwRF5T63Dn8UJyreYUcfx/dLVQ99SQBJJsw2g59V5MLmmBLe6FPML8Qu/nHJHP63WZPA8teV2GXz7vD1qW1uuamZ+qX+Gfi84b0XrpoAUaeggU3kCpbdvQ933oui7s9lvyA/CBYRjC7XYdj+36cDyfwwVVM6cDP0LTbPhw79eLDaAsw+yNsK4O5y9TuOKsScVsvfAPKaSfD1k1xLtVIqTF19yCSncTTQiSefUpILCfELMajtEGIUkOZl/jj5WQkfMkBdSnr0SdEcbzeXnuWKrf2pksaCeJA5pVFf2KFFhCm2RUKfMK5ycMGK/HTCuMWcAjoSjOcVyIHOaqVRV+FQr8DvDskcwAAAAASUVORK5CYII="
                    )
                    Log.e(TAG, "to send ${score}")
                    db.collection("classement")
                        .add(score)
                        .addOnSuccessListener { documentReference ->
                            Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                        }
                        .addOnFailureListener { e ->
                            Log.w(TAG, "Error adding document", e)
                        }
                }
            ))
            myPop.show()
        }
    }

    fun clickOnBomb() {
        val myPop: AlertDialog.Builder = AlertDialog.Builder(activity)
        myPop.setTitle("LOOSER")
        myPop.setMessage("Tu as perdu")
        myPop.setPositiveButton("Recommencer", DialogInterface.OnClickListener(
            fun(dialogInterface: DialogInterface, i: Int){
                Toast.makeText(context, "Recommencer", Toast.LENGTH_SHORT).show()
                view?.findNavController()?.navigate(R.id.navigation_jeu)
            }
        ))
        myPop.show()
    }

    fun removeFlag(i:Int){
        list.remove(i)
        list.sort()
        bombes++
        nbBombes.setText(bombes.toString())
        println(list)
        println(listBombe)
    }

    fun getBombe(b:Boolean): Int{
        return b.compareTo(false)
    }

    fun decouvre(c:Case) {
        println("coucou" + c.id.toString())
        grid[c.id].performClick()
    }

    fun startChrono(){
        chrono.setBase(SystemClock.elapsedRealtime());
        chrono.stop();
        chrono.start()
    }

    fun RequestPermission(){
        //this function will allows us to tell the user to requesut the necessary permsiion if they are not garented
        ActivityCompat.requestPermissions(
            this.requireActivity(),
            arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION,android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET),
            PERMISSION_ID
        )
    }

}