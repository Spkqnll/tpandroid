package com.example.projetdemineur.data.di.modules

import com.example.projetdemineur.data.dataSources.remote.firebase.FirebaseAuthService
import org.koin.dsl.module


object ServicesModules {
    val services = module {
        fun createFirebaseAuthService() =
            FirebaseAuthService()

        single{ createFirebaseAuthService()
        }
    }
}