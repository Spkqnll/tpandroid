package com.example.projetdemineur.data.repositories

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.projetdemineur.data.dataSources.remote.firebase.FirebaseAuthService
import com.example.projetdemineur.data.di.BaseApplication
import com.example.projetdemineur.data.di.modules.ViewModelModules.viewModels
import com.example.projetdemineur.ui.register.RegisterFragment
import com.example.projetdemineur.ui.register.RegisterViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.currentCoroutineContext
import java.security.AccessController.getContext

class AuthRepository(val firebaseAuthService: FirebaseAuthService) {

    fun getCurrentFirebaseUser() = firebaseAuthService.getCurrentFirebaseUser()


    fun signIn(email: String, password: String): MutableLiveData<FirebaseUser?> {
        val response = MutableLiveData<FirebaseUser?>()

        firebaseAuthService.signIn(email, password)
            .addOnSuccessListener {
                response.value = it.user
                Log.d(email, "AccesUserWithEmail:success")
                val user = getCurrentFirebaseUser()
                updateUI(user)
            }.addOnFailureListener {
                    Log.w(email, "AccesUserWithEmail:failure")
            }
        return response
    }

    fun signUp(email: String, password: String): MutableLiveData<FirebaseUser?>{
        val response = MutableLiveData<FirebaseUser?>()
        firebaseAuthService.signUp(email, password)
            .addOnSuccessListener {
                response.value = it.user
                Log.d(email, "createUserWithEmail:success")
                val user = getCurrentFirebaseUser()
                updateUI(user)

            }.addOnFailureListener {
                Log.d("Erreur SignUp", it.message.toString())
            }
        return response
    }
    fun updateUI(user: FirebaseUser?) {

    }

}