package com.example.projetdemineur.data.di.modules

import com.example.projetdemineur.data.repositories.AuthRepository
import com.example.projetdemineur.ui.parametre.ParametreViewModel
import com.example.projetdemineur.ui.register.RegisterViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ViewModelModules {
    val viewModels = module {

        fun createRegisterViewModel(authRepository: AuthRepository)
                = RegisterViewModel(authRepository)

        single { createRegisterViewModel(get())}

    }
}