package com.example.projetdemineur.data.di.modules

import com.example.projetdemineur.data.dataSources.remote.firebase.FirebaseAuthService
import com.example.projetdemineur.data.repositories.AuthRepository
import org.koin.dsl.module

object RepositoryModules {
    val repositories = module {
        fun createAuthRepository(firebaseAuthService: FirebaseAuthService) =
            AuthRepository(firebaseAuthService)
        single {createAuthRepository(get())}
    }
}